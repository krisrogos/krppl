# krppl

Kris Rogos Personal Python Library, KRPPL for short, is a collection of what I consider useful utilities in python.

## What is KRPPL

This is my personal project, it is a collection of useful Python helper and utility functions I've developed over the years.

## How to use this project

Currently, there are no deployed releases of KRPPL.
To use it in its current iteration I recommend just copying the individual functions you need.
A versioned release is something I might implement one day if I find it useful to do so.

## License

This project as distributed here is licensed under MIT.
For full license see the [LICENSE](LICENSE) document.

While this license is already permissive for use in commercial and closed source projects, if you are intending to use it in a professional capacity I would appreciate reaching out to me at licensing@rogos.dev to discuss the details.

## Contributions

Contributions are closed, this is a personal project and I want it to remain as such.

If you wish to make any modifications to it, I recommend creating a fork and using my `main` branch as the upstream so you can benefit from any of my future changes.
